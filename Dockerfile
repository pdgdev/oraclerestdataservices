#
# Copyright (c) 2019 Primus Delphi Group GmbH. All rights reserved.
#
# This is the Dockerfile for Oracle Rest Data Services
#
# REQUIRED FILES TO BUILD THIS IMAGE
# ----------------------------------
# (1) Download Oracle Rest Data Services from
#     http://www.oracle.com/technetwork/developer-tools/rest-data-services/downloads/index.html
#
# HOW TO BUILD THIS IMAGE
# -----------------------
# Put the downloaded file in directory "files"
# Run: 
#      $ docker build -t pdg/ords:<version> -t pdg/ords:latest . 
#
# Pull base image
# --------------------
ARG BASE_IMAGE=eclipse-temurin:17-jre-alpine
FROM ${BASE_IMAGE}

# Labels
# --------------------
LABEL maintainer="p.dirga@primus-delphi-group.com"

# Environment variables required for this build (do NOT change)
# --------------------
ENV ORACLE_PRODUCT_HOME=/u01/app/oracle/product \
    ORDS_HOME=/u01/app/oracle/product/ords \
    APEX_HOME=/u01/app/oracle/product/apex \
    APEX_VERSION=22.1.0 \
    APEX_PUBLIC_USER_NAME="APEX_PUBLIC_USER" \
    PLSQL_GATEWAY="true" \
    REST_SERVICES_APEX="true" \
    REST_SERVICES_ORDS="true"

# Copy binaries and scripts
# --------------------
COPY ["files/ords-*.zip", "scripts/*", "/tmp/"]

# Setup filesystem and oracle user
# Adjust file permissions, go to $ORDS_HOME as user 'oracle'
# to proceed with ORDS installation
# --------------------

# Add curl for Healthcheck
RUN apk add --update --no-cache --upgrade bash curl python3 \
  && rm -rf /var/cache/apk/*
  
COPY ./*.html /var/www/html/
RUN mkdir /var/www/html/ords
COPY ./*.html /var/www/html/ords
RUN mkdir /var/www/html/ords/f
COPY ./*.html /var/www/html/ords/f

RUN echo "" && \
    chmod +x /tmp/docker-image-create.sh && \
    /tmp/docker-image-create.sh

# Finalize setup
# --------------------
USER oracle
WORKDIR /home/oracle

VOLUME ["/u01/app/oracle/product"]

EXPOSE 8080

# Define healthcheck for container
# --------------------
HEALTHCHECK --start-period=10s --interval=5s --retries=5 CMD curl --fail http://localhost:8080/ords || exit 1

# Define default command to start Oracle ORDS
# --------------------
ENTRYPOINT ["/home/oracle/scripts/config-run-ords.sh"]
CMD ["run"]
