#!/bin/bash

#
# Since: October, 2019
# Author: s.lindner@primus-delphi-group.com
# Description: script to config and run ORDS
#
# Copyright (c) 2019 Primus Delphi Group GmbH. All rights reserved.
#

# (see https://stackoverflow.com/questions/44803982/how-to-run-a-bash-script-in-an-alpine-docker-container)

if [ ! -z "$MAINTENANCE" ]
then
    echo "Starting http server"
    cd /var/www/html
    python3 -m http.server 8080
elif [ ! -z "$INSTALL" ]
then
    echo "Install not implemented"
elif [ ! -z "$UNINSTALL" ]
then
    echo "Running uninstall"
    bin/ords --config /etc/ords uninstall
else
    echo Configuring ORDS
    bin/ords --config /etc/ords install --config-only --db-hostname $DB_HOSTNAME --db-port $DB_PORT --db-servicename $DB_SERVICENAME --feature-db-api true --feature-rest-enabled-sql true --feature-sdw false

    bin/ords --config /etc/ords config set jdbc.InitialLimit 10
    bin/ords --config /etc/ords config set jdbc.MaxLimit 30
    bin/ords --config /etc/ords config set jdbc.MinLimit 3
    bin/ords --config /etc/ords config set standalone.doc.root $ORDS_HOME/doc_root  
    bin/ords --config /etc/ords config set standalone.http.port 8080
    bin/ords --config /etc/ords config set standalone.static.path $APEX_HOME/$APEX_VERSION/images/
    echo "$ORDS_PASS" | bin/ords --config /etc/ords config secret --password-stdin db.password

    echo Starting ORDS
    export _JAVA_OPTIONS=-Dorg.eclipse.jetty.server.Request.maxFormContentSize=3000000 
    bin/ords --config /etc/ords serve
fi
