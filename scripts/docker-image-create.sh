#!/bin/bash

# 
# Since: October, 2019
# Author: s.lindner@primus-delphi-group.com
# Description: helper script to install ORDS in docker image
#
# Copyright (c) 2019 Primus Delphi Group GmbH. All rights reserved.
# 

# Create user oracle + group dba
addgroup -g 54322 dba
adduser -u 54321 -h /home/oracle -g dba -s /bin/bash -D oracle
USER_HOME=/home/oracle

# create folders for scripts and Move shell script + make executable
mkdir -p $USER_HOME/scripts && \
mv /tmp/config-run-ords.sh $USER_HOME/scripts/ && \
chown oracle:dba $USER_HOME/scripts/*.sh && \
chmod ug+x $USER_HOME/scripts/*.sh

# change user + group to oracle:dba for oracle product home
mkdir -p $ORACLE_PRODUCT_HOME && \
chown -R oracle:dba $ORACLE_PRODUCT_HOME

cd /tmp
unzip ords-*.zip ords.war bin/*
mv ords.war bin $USER_HOME/
chown -R oracle:dba $USER_HOME/ords.war $USER_HOME/bin

#unzip apex_*.zip apex/images/*
#chown -R oracle:dba apex

# create config directory 
mkdir -p /etc/ords && \
chown -R oracle:dba /etc/ords

# cleanup tmp
rm -rf /tmp/ords-*.zip
#rm -rf /tmp/apex_*.zip
rm -rf /tmp/*.sh
